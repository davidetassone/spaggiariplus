Changelog:
### v1.5.1 @ 25 febbraio 2019 ###
- Fix bug


### v1.5 @ 25 febbraio 2019 ###
- Sostituzione del link dei "voti dettaglio" nella "homepage" con quello dei "voti"
- Rimozione nella home page del box "Completa il tuo profilo"
- Rimozione nella home page del box "Scarica l'app"


Disponibile sul Google Web Store: https://chrome.google.com/webstore/detail/spaggiariplus/gnoclmchjddgphnehileonacaldmjgik

Contatti: davide@davidetassone.com